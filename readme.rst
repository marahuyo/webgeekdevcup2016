###################
WEB GEEK DEV CUP 2016
###################

August 20, 2016

*******************
TECH STACK
*******************
-  CodeIgniter 3.0 (as of this writing) - `<https://www.codeigniter.com/download>`_

**************************
To launch the system locally:
**************************

- Start Apache and MySQL in your cross-platform web server solution software stack (XAMPP, WAMP, LAMP, etc)
- Go to localhost/webgeek (I named my folder webgeek. If you wish to rename your folder, don't forget to modify your .htaccess file)
- Go to localhost/phpmyadmin for db (create db named 'webgeek_db')
- Modify application/config/database.php files
- Set your base url at application/config/config.php
