<body>
  <div class="container">
    <div class="section">
      <!-- header -->
      <h1 class="center-align">Welcome to Admin</h1>
      <!-- ./header -->
        <!-- cards -->
        <div class="col s12 m12 l12">
          <div class="card blue-grey darken-1">
            <div class="card-content white-text">
              <span class="card-title">Login</span>
              <?php echo form_open('admin/validate_account'); ?>
              <?php echo validation_errors();?>
              <div class="form">
                <div class="input-field col s12">
                  <i class="material-icons prefix">account_circle</i>
                  <input id="icon_prefix" type="text" class="validate">
                  <label for="icon_prefix">Email</label>
                </div>
                <div class="input-field col s12">
                  <i class="material-icons prefix">vpn_key</i>
                  <input id="icon_prefix" type="password" class="validate">
                  <label for="icon_prefix">Password</label>
                </div>
              </div>
            </div>
            <div class="center-align">
              <button class="btn waves-effect waves-light" type="submit" name="action">Login
                 <i class="material-icons right">send</i>
               </button>
            </div>
            <?php echo form_close(); ?>
            <br>
          </div>
        </div>
      </div>
      <!-- ./cards -->
    </div>
