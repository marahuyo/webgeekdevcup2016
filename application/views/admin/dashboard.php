<nav>
  <div class="nav-wrapper">
    <a href="#!" class="brand-logo">Dashboard</a>
    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down">
      <li><a href="sass.html">Sass</a></li>
      <li><a href="badges.html">Components</a></li>
      <li><a href="collapsible.html">Javascript</a></li>
      <li><a href="mobile.html">Mobile</a></li>
    </ul>
    <ul class="side-nav" id="mobile-demo">
      <li><a href="sass.html">Sass</a></li>
      <li><a href="badges.html">Components</a></li>
      <li><a href="collapsible.html">Javascript</a></li>
      <li><a href="mobile.html">Mobile</a></li>
    </ul>
  </div>
</nav>
<body>
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l12">
        <iframe src="https://embed.waze.com/iframe?zoom=12&lat=14.656828&lon=121.010850"
        width="300" height="400" style="top:5em; left:0px; right:0px; width:100%; border:none; margin:0; padding:0; z-index:999999;">
        Your browser doesn't support iframes
        </iframe>
      </div>
    </div>

    <div class="row">
      <div class="row">
         <div class="col s12 m6 l6">
           <h2 class="center-align">Recent violations</h2>
           <div class="card-panel white">
             <span class="white-text">I am a very simple card. I am good at containing small bits of information.
             I am convenient because I require little markup to use effectively. I am similar to what is called a panel in other frameworks.
             </span>
           </div>
         </div>

         <div class="col s12 m6 l6">
           <h2 class="center-align">Pending violations</h2>
           <div class="card-panel white">
             <span class="white-text">I am a very simple card. I am good at containing small bits of information.
             I am convenient because I require little markup to use effectively. I am similar to what is called a panel in other frameworks.
             </span>
           </div>
         </div>
       </div>

    </div>



  </div>
