<ul id="slide-out" class="side-nav">
      <li class="no-padding">
        <ul class="collapsible collapsible-accordion">
          <li><a href="<?php echo base_url('users')?>" style="height:48px;line-height:48px;padding:0 32px;"><i class="material-icons" style="float:left;line-height:48px;margin:0 32px 0 0;width:24px;">star</i>Dashboard</a></li>
          <li>
            <a class="collapsible-header"style="height:48px;line-height:48px;"><i class="material-icons" style="float:left;line-height:48px;margin:0 32px 0 0;width:24px;">error</i>Alerts<i class="material-icons right">arrow_drop_down</i></a>
            <div class="collapsible-body">
              <ul>
                <li> <span class="new badge">2</span><a href="<?php echo base_url('users/violations')?>">Violations</a></li>
                <li> <span class="badge">3</span><a href="<?php echo base_url('users/pending')?>">Pending Violations</a></li>
                <li> <span class="badge">1</span><a href="<?php echo base_url('users/appeal')?>">Pending Appeals</a></li>
              </ul>
            </div>
          </li>
        </ul>
      </li>
      <li><a href="#!" style="height:48px;line-height:48px;padding:0 32px;"><i class="material-icons" style="float:left;line-height:48px;margin:0 32px 0 0;width:24px;">perm_identity</i>Profile</a></li>
      <li><a href="#!" style="height:48px;line-height:48px;padding:0 32px;"><i class="material-icons" style="float:left;line-height:48px;margin:0 32px 0 0;width:24px;">power_settings_new</i> Logout</a></li>
    </ul>
    <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons" style="color:rgba(0,0,0,0.87)">menu</i></a>
  <script>
  // Initialize collapse button
 $(".button-collapse").sideNav();
 // Initialize collapsible (uncomment the line below if you use the dropdown variation)
 $('.collapsible').collapsible();
</script>
