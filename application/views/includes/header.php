<!-- HEADER -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>DevCup 2016</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php echo base_url('assets/materialize/css/materialize.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection"/>
    <!--<link href="<?php echo base_url('assets/css/style.css');?>" type="text/css" rel="stylesheet" media="screen,projection"/>-->
    <link href="<?php echo base_url('assets/js/swa/sweetalert-master/dist/sweetalert.css');?>"  type="text/css" rel="stylesheet"  >
    <link href="<?php echo base_url('assets/js/jsgrid/dist/jsgrid.min.css');?>" type="text/css" rel="stylesheet" />
    <link href="<?php echo base_url('assets/js/jsgrid/dist/jsgrid-theme.min.css');?>" type="text/css" rel="stylesheet" />
    <link href="<?php echo base_url('assets/js/data-tables/css/jquery.dataTables.min.css');?>" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo base_url('assets/js/magpop/dist/magnific-popup.css');?>" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- JS -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-3.0.0.min.js');?>"></script>
    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.min.js"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/rstaib/jquery-steps/master/build/jquery.steps.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/magpop/dist/jquery.magnific-popup.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/imagesloaded/imagesloaded.pkgd.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/masonry/masonry.pkgd.min.js');?>"></script>
  </head>
