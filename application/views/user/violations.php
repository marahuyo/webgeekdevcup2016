<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center cyan-text text-darken-3">Violations</h3>
    <br><br>
  </div>
</div>

<div class="container">
  <table class="bordered table" style="margin-bottom:100px">
    <thead>
      <tr>
          <th data-field="">Violation Number</th>
          <th data-field="">Violation</th>
          <th data-field="">Date</th>
          <th data-field="">Location</th>
          <th data-field="">Officer in Charge</th>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>
