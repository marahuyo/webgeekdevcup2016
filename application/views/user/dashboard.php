<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h3 class="header center cyan-text text-darken-3">Dashboard</h3>
    <br><br>
  </div>
</div>

<div class="container">
    <h5 class="header center">Violations</h5>
  <table class="bordered table" style="margin-bottom:100px">
    <thead>
      <tr>
          <th data-field="">Violation Number</th>
          <th data-field="">Violation</th>
          <th data-field="">Date</th>
          <th data-field="">Location</th>
          <th data-field="">Officer in Charge</th>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>

<div class="container">
    <h5 class="header center">Pending Violation</h5>
  <table class="bordered table" style="margin-bottom:100px">
    <thead>
      <tr>
          <th data-field="">Violation Number</th>
          <th data-field="">Violation</th>
          <th data-field="">Date</th>
          <th data-field="">Location</th>
          <th data-field="">Officer in Charge</th>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>

<div class="container">
    <h5 class="header center">Pending Appeal</h5>
  <table class="bordered table" style="margin-bottom:100px">
    <thead>
      <tr>
          <th data-field="">Violation Number</th>
          <th data-field="">Violation</th>
          <th data-field="">Date</th>
          <th data-field="">Location</th>
          <th data-field="">Officer in Charge</th>
      </tr>
    </thead>

    <tbody>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>
