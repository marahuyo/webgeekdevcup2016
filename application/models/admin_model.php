<?php

class Admin_model extends CI_Model{

  function check_account_db($user,$pass)
  {
      $this->db->where('type', 'a');
      $this->db->where('username', $user);
      $this->db->where('password',$pass);
      $query = $this->db->get('accounts');

      if($query->num_rows() == 1)
      {
        $row = $query->row();
        $data = array(
          'username' => $row->username,
          'userId' => $row->userId,
          'accountName' => $row->accountName,
          'is_logged_in' => true
        );
        $this->session->set_userdata($data);

        return true;
      }
      else{
        $this->session->set_flashdata('password', 'Wrong username/password.');
        return false;
      }
    }
}
